from typing import List

def _prob3(a: str) -> str:
    """Returns the first missing letters of the alphabet with O(N) time complexity."""
    temp = {x: True for x in list(a)}
    for y in list('abcdefghijklnopqrstuvyz'):
        if y not in temp:
            return y


if __name__ == "__main__":
    print(_prob3(a = "the quick brown box jumps over a lazy dog"))