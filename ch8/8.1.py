from typing import List

def _prob1(a: List[int], b: List[int]) -> List[int]:
    """Find the intersection between two arrays with O(N) time complexity"""
    temp = {x: True for x in a}
    return [x for x in b if x in temp]

if __name__ == "__main__":
    print(_prob1(a = [1, 2, 3, 4, 5], b = [0, 2, 4, 6, 8]))