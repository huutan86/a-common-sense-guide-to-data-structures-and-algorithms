from typing import List
def _prob2(a: List[str]) -> str:
    """Returns the first duplicate found in an array of string with O(N) time complexity,"""
    temp = {}
    for x in a:
        if x not in temp:
            temp[x] = True
        else:
            return x

if __name__ == "__main__":
    print(_prob2(a = ["a", "b", "c", "d", "c", "e", "f"]))