
def _prob4(s: str) -> str:
    """Returns a fist non duplicated letter of a string."""
    temp = {}
    for x in list(s):
        if x not in temp:
            temp[x] = 1
        else:
            temp[x] += 1

    for x in list(s):
        if temp[x] == 1:
            return x

if __name__ == "__main__":
    print(_prob4(s = "minimum"))