from typing import Optional
from typing import Tuple
class Node:
    def __init__(self, val: int, left: Optional["Node"] = None, right: Optional["Node"] = None) -> None:
        self.val = val
        self.left = left
        self.right = right

class BinaryTree:
    def __init__(self, root: Optional[Node] = None):
        self.root = root

    def insert_value_to_children_of_a_node_and_return_updated_version(self, value: int, current_node: Node) -> Node:
        if self.root is None:
            self.root = Node(val=value)
            return self.root

        if current_node is None:
            return Node(val=value)
        elif value < current_node.val:
            current_node.left = self.insert_value_to_children_of_a_node_and_return_updated_version(
                value=value, current_node=current_node.left)
            return current_node

        elif value > current_node.val:
            current_node.right = self.insert_value_to_children_of_a_node_and_return_updated_version(
                value=value, current_node=current_node.right)
            return current_node
        else:
            print("Node existed. Ignored!")
            return None

    def search_tree_for_value(self, value: int, current_node: Node) -> Optional[Node]:
        if current_node is None:
            return None
        elif current_node.val == value:
            return current_node
        elif current_node.val > value:
            return self.search_tree_for_value(value=value, current_node=current_node.left)
        else:
            return self.search_tree_for_value(value=value, current_node=current_node.right)

    def inorder_traversal(self, current_node: Node) -> None:
        if current_node is None:
            return
        else:
            self.inorder_traversal(current_node.left)
            print(f"{current_node.val}")
            self.inorder_traversal(current_node.right)

    def preorder_traversal(self, current_node: Node) -> None:
        if current_node is None:
            return
        else:
            print(f"{current_node.val}")
            self.preorder_traversal(current_node.left)
            self.preorder_traversal(current_node.right)

    def postorder_traversal(self, current_node: Node) -> None:
        if current_node is None:
            return
        else:
            self.postorder_traversal(current_node.left)
            self.postorder_traversal(current_node.right)
            print(f"{current_node.val}")

    def delete_value_from_children_of_a_node_and_return_its_replacement(self, value: int, current_node: Optional[Node]) -> Optional[Node]:
        """Deletes a value from a node and returns its replacement.

        This will be much simpler if we have a reference to the parent from the child.
        """
        if current_node is None:
            return None
        elif current_node.val > value:
            current_node.left = self.delete_value_from_children_of_a_node_and_return_its_replacement(
                value=value, current_node=current_node.left)
            return current_node
        elif current_node.val < value:
            current_node.right = self.delete_value_from_children_of_a_node_and_return_its_replacement(
                value=value, current_node=current_node.right)
            return current_node
        elif current_node.val == value:
            if current_node.left is None:
                return current_node.right
            elif current_node.right is None:
                return current_node.left
            else:
                current_node.right = self._replace_node_to_replace_with_successor_return_current_node_with_replaced_left_child(
                    current_node=current_node.right, node_to_replace=current_node)
                return current_node

    def _replace_node_to_replace_with_successor_return_current_node_with_replaced_left_child(self, current_node: Node, node_to_replace: Node) -> Node:
        if current_node.left is not None:
            # Use the recursion from the inner call and assign it to the left child.
            current_node.left = self._replace_node_to_replace_with_successor_return_current_node_with_replaced_left_child(
                current_node=current_node.left, node_to_replace=node_to_replace)
            return current_node
        else:
            # Current node is the successor
            node_to_replace.val = current_node.val
            return current_node.right

    def find_greatest_number(self, current_node: Node) -> int:
        """Problem 15.3"""
        if current_node.right is None:
            return current_node.val
        else:
            return self.find_greatest_number(current_node=current_node.right)

if __name__ == "__main__":
    tree = BinaryTree(root=None)
    node_values = [50, 25, 75, 10, 33, 4, 11, 30, 40, 55, 56, 89, 52, 61, 82, 95]

    for value in node_values:
        tree.insert_value_to_children_of_a_node_and_return_updated_version(value=value, current_node=tree.root)

    res = tree.search_tree_for_value(value=34, current_node=tree.root)

    tree.inorder_traversal(current_node=tree.root)
    tree.delete_value_from_children_of_a_node_and_return_its_replacement(value=4, current_node=tree.root)
    tree.delete_value_from_children_of_a_node_and_return_its_replacement(value=10, current_node=tree.root)
    tree.delete_value_from_children_of_a_node_and_return_its_replacement(value=56, current_node=tree.root)
    tree.delete_value_from_children_of_a_node_and_return_its_replacement(value=50, current_node=tree.root)

    tree.delete_value_from_children_of_a_node_and_return_its_replacement(value=56, current_node=tree.root)
    print("\n")
    tree.inorder_traversal(current_node=tree.root)

    print(f"Max value in binary tree {tree.find_greatest_number(current_node=tree.root)}")

    print("\n")
    tree.preorder_traversal(current_node=tree.root)

    print("\n")
    tree.postorder_traversal(current_node=tree.root)


