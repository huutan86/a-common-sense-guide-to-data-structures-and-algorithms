from typing import Dict
from typing import Tuple

def unique_paths(rows: int, cols: int) -> int:
    if rows == 1 or cols == 1:
        return 1
    return unique_paths(rows - 1, cols) + unique_paths(rows, cols - 1)

def unique_paths2(rows:int, cols: int, memo: Dict[Tuple[int, int], int]) -> int:
    if rows == 1 or cols == 1:
        return 1
    if (rows, cols) not in memo:
        memo[(rows, cols)] = unique_paths2(rows - 1, cols, memo) + unique_paths2(rows, cols - 1, memo)
    return memo[(rows, cols)]

if __name__ == "__main__":
    print(unique_paths2(rows=200, cols=300, memo={}))