from typing import Dict
def golomb1(n: int) -> int:
    if n == 1:
        return 1
    return 1 + golomb1(n - golomb1(golomb1(n - 1)))

def golomb2(n: int, memo: Dict[int, int]) -> int:
    """Memoization version"""
    if n == 1:
        return 1
    if n not in memo:
        memo[n] = 1 + golomb2(n - golomb2(golomb2(n-1, memo), memo), memo)
    return memo[n]

if __name__ == "__main__":
    print(golomb2(300, {}))