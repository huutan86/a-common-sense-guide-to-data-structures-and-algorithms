from typing import List
def partition_and_return_pivot_index(a: List[int], first_idx: int, last_idx) -> int:
    """Performs partitionting the array and returns the pivot index (the index of the item that has correct position)."""
    first_larger_idx_from_left = first_idx
    pivot_idx = last_idx
    first_smaller_idx_from_right = last_idx - 1
    pivot_value = a[pivot_idx]
    while True:
        while a[first_larger_idx_from_left] < pivot_value:
            first_larger_idx_from_left += 1
        
        while a[first_smaller_idx_from_right] > pivot_value:
            first_smaller_idx_from_right -= 1
        
        if first_larger_idx_from_left < first_smaller_idx_from_right:
            a[first_smaller_idx_from_right], a[first_larger_idx_from_left] = a[first_larger_idx_from_left],  \
                                                                             a[first_smaller_idx_from_right]
        else:
            a[pivot_idx], a[first_larger_idx_from_left] = a[first_larger_idx_from_left], a[pivot_idx]
            pivot_idx = first_larger_idx_from_left
            break

    return pivot_idx

def quicksort(a: List[int], left_idx: int, right_idx: int) -> None:
    """Sorts a list of integer in ascending order."""
    if  left_idx >= right_idx:
        # Only element left
        return

    p = partition_and_return_pivot_index(a, left_idx, right_idx)
    quicksort(a, left_idx=left_idx, right_idx=p - 1)
    quicksort(a, left_idx=p + 1, right_idx=right_idx)

if __name__ == "__main__":
    a = [0, 5, 2, 1, 6, 3]
    quicksort(a, left_idx=0, right_idx=len(a) - 1)
    print(a)
    