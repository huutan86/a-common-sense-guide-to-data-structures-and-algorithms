from typing import List
from _util import quicksort

def _prob1(a: List[int]) -> int:
    """Returns the products of 3 largest elements."""
    quicksort(a, left_idx = 0, right_idx=len(a) - 1)
    return a[-1] * a[-2] * a[-3]

if __name__ == "__main__":
    print(_prob1(a = [0, 5, 2, 1, 6, 3]))