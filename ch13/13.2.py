from typing import List
from _util import quicksort
def _prob2(a: List[int], left_idx: int, right_idx: int) -> int:
    """Find the missing index in the array in O(N) time using a method similar to quick select."""
    quicksort(a, left_idx=left_idx, right_idx=right_idx)
    return next(x for x in range(len(a)) if x != a[x])

if __name__ == "__main__":
    a = [9, 3, 2, 5, 6, 7, 1, 0, 4]
    print(_prob2(a=a, left_idx=0, right_idx=len(a) - 1))