from typing import List
from _util import quicksort

def _bubble_sort(a: List[int]) -> None:
    for last_idx in range(len(a) - 1, 1, -1):
        for i in range(0, last_idx):
            if a[i] > a[i + 1]:
                a[i], a[i + 1] = a[i+1], a[i]

def _find_max_o_n2(a: List[int]) -> int:
    """Uses bubble sort like to find the max."""
    _bubble_sort(a)
    return a[-1]

def _find_max_o_n_log_n(a: List[int]) -> int:
    quicksort(a, left_idx=0, right_idx=len(a) - 1)
    return  a[-1]

def _find_max_o_n(a: List[int]) -> int:
    for i in range(0, len(a) - 1):
        if a[i] > a[i + 1]:
            a[i], a[i + 1] = a[i + 1], a[i]
    return a[-1]


if __name__ == "__main__":
    a = [9, 3, 2, 5, 6, 7, 1, 0, 4]
    print(_find_max_o_n2(a.copy()))
    print(_find_max_o_n_log_n(a.copy()))
    print(_find_max_o_n(a.copy()))