from typing import Optional

class Node:
    def __init__(self, value: int, next: Optional["Node"] = None):
        self.value = value
        self.next = next

class LinkedList:
    def __init__(self, first_node: Node):
        self.first_node = first_node

    def insert_at_index(self, index: int, value: int):
        """Inserts a value at a certain index."""
        new_node = Node(value=value, next=None)
        if index == 0:
            new_node.next = self.first_node
            self.first_node = new_node
        else:
            current_idx = 0
            current_node = self.first_node
            # Get the node before the node to insert.
            while current_idx < index - 1:
                current_node = current_node.next
                current_idx += 1

            if current_node.next is not None:
                new_node.next = current_node.next
            else:
                new_node.next = None
            current_node.next = new_node

    def delete_at_index(self, index: int) -> None:
        """Delete a node at an index."""
        if index == 0:
            self.first_node = self.first_node.next
        else:
            current_node = self.first_node
            current_idx = 0
            while current_idx < index - 1:
                current_node = current_node.next
                current_idx += 1
                if current_node is None:
                    break

            if current_idx < index - 1:
                print(f" The linked list did not have node at index {index}")
                return
            else:

                if current_node.next is not None:
                    next_node_after_delete = current_node.next.next
                else:
                    next_node_after_delete = None

                current_node.next = next_node_after_delete

    def print_all_nodes(self):
        """14.1."""
        current_node = self.first_node
        while current_node is not None:
            print(f"value = {current_node.value}")
            current_node = current_node.next
        print(f"\n")

    def print_last_element(self) -> None:
        """14.3"""
        current_node = self.first_node
        while current_node.next is not None:
            current_node = current_node.next
        print(f"{current_node.value}")

    def reverse_list(self) -> None:
        """14.4.
        To reverse the list, we need to keep track of the previous node, current node, and the next node.
        At the current node, what we need to do is:
        1) Get the reference to the .next node as backup (to prepare for the next iteration)
        2) Set the .next attribute to the previous node
        3) Set the current node as the prev node.
        4) Move on to the next node from the backup if there is a follow up node.
        """
        prev_node = None
        current_node = self.first_node
        while True:
            next_node = current_node.next

            current_node.next = prev_node

            if next_node is None:
                break

            prev_node = current_node
            current_node = next_node

        self.first_node = current_node


    def node_at_index(self, index: int) -> Node:
        current_node = self.first_node
        current_idx = 0
        while current_idx < index:
            current_node = current_node.next
            current_idx += 1
        return current_node

    def delete_node_without_root_node(self, node: Node) -> None:
        """14.5.
        Delete a node without knowing its predecessor. We do this by keep it but replace its decendant with it.
        """
        node.value = node.next.value
        node.next = node.next.next

if __name__ == "__main__":
    ll = LinkedList(first_node=Node(value="blue"))
    ll.insert_at_index(index=1, value="green")
    ll.insert_at_index(index=2, value="red")
    ll.print_all_nodes()

    ll.insert_at_index(index=0, value="yellow")
    ll.print_all_nodes()

    ll.insert_at_index(index=2, value="purple")
    ll.print_all_nodes()

    ll.delete_at_index(index=2)
    ll.print_all_nodes()

    ll.print_last_element()

    ll.reverse_list()
    ll.print_all_nodes()

    temp_node = ll.node_at_index(index=1)
    ll.delete_node_without_root_node(node=temp_node)
    ll.print_all_nodes()