from typing import Optional

class Node:
    def __init__(self, value: int, next: Optional["Node"] = None, prev: Optional["Node"] = None):
        self.value = value
        self.next = next
        self.prev = prev

class DoublyLinkedList:
    def __init__(self, first_node: Optional[Node] = None, last_node: Optional[Node] = None) -> None:
        self.first_node = first_node
        self.last_node = last_node


    def insert_at_end(self, value: int) -> None:
        """Inserts a node at the end of the doubly linked list"""
        if self.first_node is None:
            new_node = Node(value=value, next=None, prev=None)
            self.first_node = new_node
            self.last_node = new_node
        else:
            new_node = Node(value=value, next=None, prev=self.last_node)
            # Update the link of the old last node.
            self.last_node.next = new_node

            # Set the new node to be the new last node.
            self.last_node = new_node

    def remove_from_front(self) -> None:
        # Make sure the 2nd node is updated
        if self.first_node.next is not None:
            self.first_node.next.prev = None
        self.first_node = self.first_node.next


    def print_all_nodes(self) -> None:
        current_node = self.first_node
        while current_node is not None:
            print(f"{current_node.value}")
            current_node = current_node.next

        print("\n")

    def print_in_reverse_order(self) -> None:
        """14.2"""
        current_node = self.last_node
        while current_node is not None:
            print(f"{current_node.value}")
            current_node = current_node.prev

        print("\n")


if __name__ == "__main__":
    ll = DoublyLinkedList()
    ll.insert_at_end(value="Bob")
    ll.insert_at_end(value="Jill")
    ll.insert_at_end(value="Emily")
    ll.insert_at_end(value="Sue")
    ll.print_all_nodes()

    ll.remove_from_front()
    ll.print_all_nodes()
    ll.print_in_reverse_order()
