from typing import List

def _prob1(a: List[str]) -> int:
    if len(a) == 0:
        return 0
    else:
        return len(a[0]) + _prob1(a[1:])
    
if __name__ == "__main__":
    print(_prob1(a = ["ab", "c", "def", "ghij"]))