def _prob3(n: int) -> int:
    """Returns the N-triangle number"""
    if n == 1:
        return 1
    else:
        return n + _prob3(n - 1)

if __name__ == "__main__":
    print(_prob3(n=7))