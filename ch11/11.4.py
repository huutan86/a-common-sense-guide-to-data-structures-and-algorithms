def _prob4(s: str, i: int = 0) -> int:
    """Returns the index of the letter that contains the letter x"""
    if s[0] == "x":
        return i
    else:
        return _prob4(s[1:], i = i+1)
    
if __name__ == "__main__":
    print(_prob4(s = "abcdefghijklmnopqrstuvwxyz"))
