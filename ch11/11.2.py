from typing import List

def _prob2(a: List[int]) -> List[int]:
    """Returns a substring of even number in the input string."""
    if len(a) == 0:
        return []
    out = []
    if a[0] % 2 == 0:
        out.append(a[0])
    out.extend(_prob2(a[1:]))
    return out

if __name__ == "__main__":
    print(_prob2(a = [12, 3, 1, 2, 4, 7, -2]))