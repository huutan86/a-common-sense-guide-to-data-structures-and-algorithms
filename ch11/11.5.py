
def _prob5(n_rows: int, n_cols: int) -> int:
    if n_rows == 1:
        return 1
    if n_cols == 1:
        return 1
    return _prob5(n_rows - 1, n_cols) + _prob5(n_rows, n_cols - 1)

if __name__ == "__main__":
    print(_prob5(n_rows=3, n_cols=7))